package com.wagner.bean;

import java.lang.reflect.Type;
import java.net.URI;
import java.util.ArrayList;
import java.util.List;

import javax.faces.bean.ApplicationScoped;
import javax.faces.bean.ManagedBean;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.UriBuilder;

import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.google.gson.reflect.TypeToken;
import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.WebResource;
import com.sun.jersey.api.client.config.ClientConfig;
import com.sun.jersey.api.client.config.DefaultClientConfig;
import com.wagner.pojo.User;

@ManagedBean(name ="userService")
@ApplicationScoped
public class UserService {

	ClientConfig config = new DefaultClientConfig();
	Client client       = Client.create(config);
	WebResource service = client.resource(getBaseURI());
	WebResource restWS  = service.path("/api");
	
	public List<User> listUsers(){
		List<User> users = new ArrayList();
		 
        Gson gson = new Gson();
        String Json = restWS.path("user").accept(MediaType.APPLICATION_JSON).get(String.class);
        JsonParser parser = new JsonParser();
        JsonObject rootObejct = parser.parse(Json).getAsJsonObject();
        JsonElement userElement = rootObejct.get("user");
        Type userListaType = new TypeToken<List<User>>() {}.getType();
        users = gson.fromJson(userElement, userListaType);
        
        return  users;
	}
	
	private static URI getBaseURI() {
		return UriBuilder.fromUri("http://localhost:8080/DataRest").build();
	}
	
}
