package com.wagner.bean;

import java.io.Serializable;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;

import com.wagner.pojo.User;

@ManagedBean(name="dtUserView")
@ViewScoped
public class UserView implements Serializable{

	private List<User> users;
	
	@ManagedProperty("#{userService}")
	private UserService userService;
	
	@PostConstruct
	public void init(){
		users = userService.listUsers();
	}
	
}
