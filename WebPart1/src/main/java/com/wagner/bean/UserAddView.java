package com.wagner.bean;

import javax.faces.bean.ManagedBean;
import javax.ws.rs.core.MediaType;

import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;
import com.sun.jersey.api.client.config.ClientConfig;
import com.sun.jersey.api.client.config.DefaultClientConfig;
import com.sun.jersey.api.representation.Form;
import com.wagner.pojo.User;

@ManagedBean(name="userAddView")
public class UserAddView {

	private Long id;
	private String name;
	private String description;
	
	final String rest = "http://localhost:8080/DataRest/api";
	final String PERS   = "user";
	
	ClientConfig config = new DefaultClientConfig();
	Client client       = Client.create(config);
	WebResource service = client.resource("");
	WebResource restWS  = service.path(rest);
	
	public String processAction() {
		
		User user = new User();
		user.setDescription(description);
		user.setName(name);
		
		Form form = new Form();
		form.add("name", name);
		form.add("description", description);
		System.out.println(id + "--" + name + "--" + description);
		ClientResponse response = restWS.path(PERS).type(MediaType.APPLICATION_JSON)
								   .post(ClientResponse.class, user);
        return "index";
    }
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	
	
}
