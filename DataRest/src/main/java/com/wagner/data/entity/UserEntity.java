package com.wagner.data.entity;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.wagner.data.utils.BaseEntities;

@Entity
@Table(name="User")
public class UserEntity extends BaseEntities<Long>{

	
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 3482646100418569605L;

	@ManyToMany
	@JoinTable(name="user_permission",joinColumns={@JoinColumn(name="user_id")},
	inverseJoinColumns={@JoinColumn(name="permission_id")})
	List<PermissionEntity> permissions;
	
	@ManyToOne
	@JoinColumn(name="department_id")
	DepartmentEntity department;
	
	@Column(name="name",nullable=false,unique=false)
	String name;
	@Column(name="description",nullable=false,unique=false)
	String description;
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	
}
