package com.wagner.data.entity;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ManyToMany;
import javax.persistence.Table;

import com.wagner.data.utils.BaseEntities;

@Entity
@Table(name="Permission")
public class PermissionEntity extends BaseEntities<Long>{

	/**
	 * 
	 */
	private static final long serialVersionUID = 3897078839660803275L;
	@ManyToMany(mappedBy="permissions")
	List<UserEntity> users;
	@Column(name="name",nullable=false,unique=false)
	String name;
	@Column(name="description",nullable=false,unique=false)
	String description;
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	
	
	
}
