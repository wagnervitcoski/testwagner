package com.wagner.data.entity;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.wagner.data.utils.BaseEntities;

@Entity
@Table(name="Department")
public class DepartmentEntity extends BaseEntities<Long>{

	
	/**
	 * 
	 */
	private static final long serialVersionUID = 7875494488494047720L;

	@OneToMany(mappedBy="department",
			targetEntity=UserEntity.class,
			fetch = FetchType.LAZY,
			cascade=CascadeType.ALL)
	List<UserEntity> users;
	
	@Column(name="name",nullable=false,unique=false)
	String name;
	@Column(name="description",nullable=false,unique=false)
	String description;
	
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	
}
