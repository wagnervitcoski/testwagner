package com.wagner.data.repo;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import com.wagner.data.entity.DepartmentEntity;

@RepositoryRestResource(collectionResourceRel="dep",path="dep")
public interface IDepartmentRepository extends JpaRepository<DepartmentEntity, Long>{

}
