package com.wagner.data.repo;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import com.wagner.data.entity.UserEntity;

@RepositoryRestResource(collectionResourceRel="user",path="user")
public interface IUserRepository extends JpaRepository<UserEntity, Long>{

	
	
}
