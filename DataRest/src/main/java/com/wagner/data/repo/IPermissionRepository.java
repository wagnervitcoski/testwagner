package com.wagner.data.repo;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import com.wagner.data.entity.PermissionEntity;

@RepositoryRestResource(collectionResourceRel="perm",path="perm")
public interface IPermissionRepository extends JpaRepository<PermissionEntity, Long>{

}
