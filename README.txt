All CRUDs have been developed using Spring-Data with Rest (RepositoryRestResource), therefore there resources are automatically implemented and exposed by the framework.
To access the available resources, please check http://localhost:8080/DataRest/api
http://localhost:8080/DataRest/api/user
http://localhost:8080/DataRest/api/dep
http://localhost:8080/DataRest/api/perm

The webapp is available in the following link http://localhost:8080/WebPart/

To make the apps available on Tomcat, compile the projects using maven (run mvn package in both projects), which will generate the war file in their corresponding target folders.
Copy the war files to your tomcat container.
The application is using HSQLDB.

Not no all the requirements have been implemented, I was not able to complete the web part.
